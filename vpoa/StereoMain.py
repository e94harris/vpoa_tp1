# -*- coding: utf-8 -*-
from Calibration import StereoCalibration
from Rectification import StereoRectification
import cv2 as cv
import numpy as np

if __name__ == '__main__':
    # Calibration
    calib = StereoCalibration(7, 6, 1.08, 'chessboard')
    _, cameraMatrixLeft, distCoeffsLeft, cameraMatrixRight, distCoeffsRight, imageSizeLeft, R, T = calib.calibrate()
    # Visualization
    # rectification.display(left, right)
    # Rectification
    rectification = StereoRectification(cameraMatrixLeft, distCoeffsLeft,
                                        cameraMatrixRight, distCoeffsRight,
                                        imageSizeLeft, R, T)
    left = cv.imread('data/stereo/MinnieRawLeft.png', cv.IMREAD_GRAYSCALE)
    right = cv.imread('data/stereo/MinnieRawRight.png', cv.IMREAD_GRAYSCALE)
    # 3D reconstruction
    rectification.displayDisparity(left, right)
