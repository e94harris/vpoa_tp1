# -*- coding: utf-8 -*-
from Calibration import MonoCalibration
from Rectification import MonoRectification

# Acquisition
if __name__ == '__main__':
    calib = MonoCalibration(4, 11, 0.04, 'asymmetric_circles')
    calib.acquire()
    # Calibration
    _, _, _, img_size = calib.calibrate()

    # Visualization
    calib.visualizeBoards()
    calib.plotRMS()

    rectification = MonoRectification(calib.cameraMatrix, calib.distCoeffs, img_size)
    # Rectification
    rectification.display()

